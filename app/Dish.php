<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
    public function ingredients()
    {
        return $this->belongsToMany('App\Ingredient')->withPivot('quantity');
    }
    public function type()
    {
        return $this->belongsTo('App\Type');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
