<?php

namespace App\Policies;

use App\User;
use App\Dish;
use Illuminate\Auth\Access\HandlesAuthorization;

class DishPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the product.
     *
     * @param  \App\User  $user
     * @param  \App\Dish  $dish
     * @return mixed
     */
    public function view(User $user, Dish $dish)
    {
        
        return true;
    }
    public function show(User $user, Dish $dish)
    {
        return true;
    }
    public function create(User $user)
    {
        if ($user) {
            return true;
        }
        return false;
    }
    public function delete(User $user, Dish $dish)
    {
        if ($user->id == $dish->user_id) {
            return true;
        }
        return false;
    }
}
