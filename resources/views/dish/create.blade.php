@extends('layouts.app')
    @section('content')
    <div class="jumbotron text-center">
        <p>Alta de Plato</p>
    </div>
        
    <form action="/dishes" method="POST">
    {{ csrf_field() }}
        <div class="form-group">
            <label>Nombre:</label><input type="text" name="name">
            {{ $errors->first('name') }}
        </div>
        <div class="form-group">
            <label>Description:</label><textarea class="form-control" rows="5" name="description" id="description"></textarea>
            {{ $errors->first('description') }}
        </div>
        <div class="form-group">
            <select name="type">
                @foreach($types as $type)
                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                @endforeach
            </select>
            {{ $errors->first('type') }}
        </div>
        <div class="form-group">
            <input type="submit" name="Crear" value="Crear">
        </div>
    </form>

    @stop