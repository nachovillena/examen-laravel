@extends('layouts.app')
    @section('content')
    <div class="jumbotron text-center">
        <p>Lista de Platos</p>
    </div>
    @can('create', $dishes)
        <a href="/dishes/create" >Nuevo</a>
        @endcan
        <table class="table"> 
        <tr>
            <th>Usuario</th><th>Tipo de plato</th><th>Nombre</th><th>Descripcion</th><th>Acción</th>
        </tr>
        @foreach($dishes as $dish)
        <tr>
        <!-- observa que podemos recuperar atributos de dos modos: -->
            <td>{{ $dish->user->name }}</td>
            <td>{{ $dish->type->name }}</td>
            <td>{{ $dish->name }}</td>
            <td>{{ $dish->description }}</td>
            <td>
            @can('delete', $dish)
                <form method="post" action="/dishes/{{ $dish->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                
                <input type="submit" value="Borrar">
            
            
                <a href="/dishes/{{ $dish->id }}/edit">Editar</a>
            @endcan
                <a href="/dishes/{{ $dish->id }}">Ver</a>
                </form>
            </td>
        </tr>
        @endforeach
        </table>
        @can('create', $dish)
        <a href="/dishes/create" >Nuevo</a>
        @endcan

    <div class="pagination text-center">{!! $dishes->links() !!}</div>

    @stop