@extends('layouts.app')
    @section('content')
    <div class="jumbotron text-center">
        <p>Detalle de {{$dish->name}}</p>
    </div>
    <p><b>ID:</b> {{ $dish->id }}</p>
    <p><b>Descripción:</b> {{ $dish->description }}</p>
    <p><b>Tipo:</b> {{ $dish->type->name }}</p>
    <p><b>Usuario:</b> {{ $dish->user->name }}</p>
    <p><b>Ingredientes:</b></p>
    <table class="table"> 
        <tr>
            <th>Id</th><th>Nombre</th>
        </tr>
        @foreach($dish->ingredients as $ingredient)
        <tr>
        <!-- observa que podemos recuperar atributos de dos modos: -->
            <td>{{ $ingredient->id }}</td>
            <td>{{ $ingredient->name }}</td>
        </tr>
        @endforeach
        </table>

    @stop